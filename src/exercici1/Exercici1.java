/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercici1;

/**A program that replaces the ‘P’s’ of a String with the symbol ‘?’ Like this ‘PAAPDDAAPDDD’.
 *
 * @author Awan
 */
public class Exercici1 {

    public static void main(String[] args) {
        String word = "PAAPDDAAPDD";
        String wordReplace = word.replace('P','?');
        System.out.println("Original String is : " + word);
        System.out.println("String after replacing 'P' with '?': " + wordReplace);
        System.out.println("Hola");
    }
}